# New ZFS Replication Program
# Kory Wegmeyer 
# 8/12/19
# 11/11/20

import time
from subprocess import run, PIPE, Popen, check_output
import socket

def nixtime():
    return int(time.time())

def gs(ds):
    '''
    Gets all zrepi snaps and their unixcreation for a given dataset.
    '''
    snaps = []
    out = run(["zfs","list", "-Hrt", "snapshot", ds], stdout=PIPE)
    out = out.stdout.decode('utf-8')
    if len(out) < 1:
        raise Exception("Could not find snaps for specified dataset: " + ds)
    out = out.split("\n")
    del out [-1]
    for snap in out:
        line = snap.split("\t")
        prop = gp(line[0], "zrepi:unixcreation")
        if prop['value'] != '-':
            snaps.append(prop)
    if len(snaps) < 1:
        raise Exception("Could not find and zrepi snaps for specified dataset: " + ds)
        #print(line)
        #snaps.append({"dataset": line[0], "unixcreated"})
    return snaps

def grs(ds, remote, keypath):

    '''
    Gets all zrepi snaps and their unixcreation for a given remote dataset.
    '''
    snaps = []
    out = run(["ssh", "-i", keypath, remote, "zfs", "list", "-Hrt", "snapshot", ds], stdout=PIPE)
    out = out.stdout.decode('utf-8')
    if len(out) < 1:
        raise Exception("Could not find snaps for specified dataset: " + ds)
    out = out.split("\n")
    del out [-1]
    for snap in out:
        line = snap.split("\t")
        prop = grp(line[0], "zrepi:unixcreation", remote, keypath)
        if prop['value'] != '-':
            snaps.append(prop)
    if len(snaps) < 1:
        raise Exception("Could not find and zrepi snaps for specified dataset: " + ds)
        #print(line)
        #snaps.append({"dataset": line[0], "unixcreated"})
    return snaps

def gp(ds, prop):
    '''
    Get property from zfs dataset.
    Use fully qualified names.
    Supports snapshots with data/testset@snapname syntax.
    '''
    out = run(["zfs","get","-H", prop, ds], stdout=PIPE)
    out = out.stdout.decode('utf-8')
    out = out.split("\n")
    del out [-1]
    if len(out) != 1:
        raise Exception("Number of returned properties is not equal to one.")
    out = out[0].split("\t")
    return {"dataset": out[0], "prop": out[1], "value":out[2]}

def grp(ds, prop, remote, keypath):
    '''
    Get remote property from zfs dataset.
    Use fully qualified names.
    Supports snapshots with data/testset@snapname syntax.
    '''
    out = run(["ssh", "-i", keypath, remote, "zfs","get","-H", prop, ds], stdout=PIPE)
    out = out.stdout.decode('utf-8')
    out = out.split("\n")
    del out [-1]
    if len(out) != 1:
        raise Exception("Number of returned properties is not equal to one.")
    out = out[0].split("\t")
    return {"dataset": out[0], "prop": out[1], "value":out[2]}

def cs(ds, name, time=str(nixtime())):
    '''
    Create ZFS snapshot of target dataset.
    '''
    snaps = run(["zfs", "snapshot", "-o", "zrepi:unixcreation=" + time, ds + "@" + name ], stdout=PIPE)
    snaps = snaps.stdout.decode('utf-8')

def rep_init(localds, remote, remoteds, keypath):
    '''
    Inits the ZFS send and receive.  "replication, initial"

    Use this for performing the first snap replication.
    
    The snap locally will be replicated remotely.

    local ds (dataset) must be fully qualaified snapshots.
    '''
    print("Running full backup")
    # Get the creation time
    unixcreation = gp(localds, "zrepi:unixcreation")['value']
    # Write the source snap into the parent as the lastrep property
    parent = localds.split("@")[0]

    run(["zfs", "set", "zrepi:lastrep=" + str(unixcreation), parent], stdout=PIPE)
    send = Popen(["zfs", "send", "-p", localds], stdout=PIPE)
    recv = check_output(["ssh", "-i", keypath, remote, "zfs", "recv", "-F", remoteds], stdin=send.stdout)
    #recv = check_output(["ssh", "-i", keypath, remote, "zfs", "recv", "-F", "-x", "sharenfs", remoteds], stdin=send.stdout)
    send.wait()


def rep_inc(oldsnap, newsnap, remote, remoteds, keypath):
    '''
    Performs an incremental zfs send and receive.  "replication, incremental"
    
    Use this to replicate data after the initial send has been completed.

    The snap locally will be replicated remotely.

    old and new snap (dataset) must be fully qualified snapshots.
    '''
    # Get the creation time
    unixcreation = gp(newsnap, "zrepi:unixcreation")['value']
    # Write the source snap into the parent as the lastrep property
    parent = newsnap.split("@")[0]
    run(["zfs", "set", "zrepi:lastrep=" + str(unixcreation), parent], stdout=PIPE)
     
    send = Popen(["zfs", "send", "-p", "-i", oldsnap, newsnap], stdout=PIPE)
    recv = check_output(["ssh", "-i", keypath, remote, "zfs", "recv", "-F", remoteds], stdin=send.stdout)
    #recv = check_output(["ssh", "-i", keypath, remote, "zfs", "recv", "-F", "-x", "sharenfs", remoteds], stdin=send.stdout)
    send.wait()


def needs_init(ds):
    '''
    Determine if the dataset to be replicated needs init.  You can also think of this as a check
    to see if we can do an incremental replication vs a full.
    '''
    # Check if the lastrep property exists.  If it does, that suggests weve already done a replication.
    print("Checking if init is needed...") 
    if gp(ds, 'zrepi:lastrep')['value'] == '-':
        print(gp(ds, 'zrepi:lastrep')['value'])
        return True
    else:
        print(gp(ds, 'zrepi:lastrep')['value'])
        return False

def cleanup(localds, maxage, remote, remoteds, keypath):
    '''
    Deletes old snaps from both servers:
    ds is the local datastore to look for snaps in
    remote is the remote server ipaddress
    remoteds is the remote conterpart of localds
    '''
    # Process local
    snaps = gs(localds)
    for snap in snaps:
        if nixtime() - int(snap['value']) > maxage and snap['prop'] == 'zrepi:unixcreation':
            # Delete it
            print("Local deletes:")
            print(snap['dataset'])
            run(["zfs", "destroy", snap['dataset']], stdout=PIPE)
    # Process remote
    snaps = grs(remoteds, remote, keypath)
    for snap in snaps:
        if nixtime() - int(snap['value']) > maxage and snap['prop'] == 'zrepi:unixcreation':
            # Delete it
            print("Remote deletes:")
            print(snap['dataset'])
            run(["ssh", "-i", keypath, remote, "zfs", "destroy", snap['dataset']], stdout=PIPE)


def main(ds, remote, remoteds, keypath, maxage):
    '''
    Run this program from a shell or cron.
    '''
    import socket                                                          
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)                        
    s.settimeout(3)                                                             
    try:                                                                   
        s.connect((remote, 22))
        #print("Port 22 reachable")
    except socket.error as e:                                                                                
        print("Error on connect: %s" % e)                                                                     
        raise ValueError("backup target down!")             
    s.close()
    # Get timestamp
    stamp = str(nixtime())
    # Get the current snapshot layout for this dataset.  This only includes snaps created by this tool.
    if needs_init(ds):
        cs(ds, stamp, stamp)
        rep_init(ds + "@" + stamp, remote, remoteds, keypath)
    else:
        cs(ds, stamp, stamp)
        # Since we dont need init we can poll the parent for the lastrep property
        lastrep = gp(ds, 'zrepi:lastrep')['value']
        rep_inc(ds + "@" + lastrep, ds + "@" + stamp, remote, remoteds, keypath)
    # Cleanup
    # Don't run cleanup
    # cleanup(ds, maxage, remote, remoteds, keypath)

if __name__ == "__main__":
    import sys
    main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], int(sys.argv[5]))
