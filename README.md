# zrepi_ng
Next generation zfs replication script.

## What It Does
This zrepi_ng performs three major tasks each time it is run:

1.  Takes a zfs snapshot.
2.  Replicate that snapshot to a remote system running ZFS.
3.  Prune snapshots older than specified max_snapage.

## Usage
python3 zrepi_ng.py source_dataset remote_server remote_dataset ssh_keypath max_snapage

Several lines like this can be added to a crontab to replicate many different datasets each with their own retention period.

*  source_dataset = The local ZFS dataset to replicate.  e.g. data/foo
*  remote_server = The IP or FQDN of a remote server running ZFS to replicate data to.  e.g. 192.168.1.100
*  remote_dataset = The remote ZFS dataset to replicate to.  e.g. remotedata/foo
*  ssh_keypath = The path on the local system to a passwordless private ssh keyfile used to authenticate agains the remote_server.
*  max_snapage = The maximum age in seconds that a snap should exist for.  Once a snap exceds this age it is destroyed.  

When zrepi_ng is run, the given source_dataset is checked to see if it has been replicated by zrepi_ng before.
Two user properties in ZFS are used, zrepi:unixcreation and zrepi:lastrep.  zrepi:unixcreation is used to store the unix timestamp of creation for each snapshot.
zrepi:lastrep contains the unix timestamp of the last source snapshot that was *attempted* to be replicated.  Together these properties allow zrepi_ng to track
the state of each dataset.  This is important for detecting if a full or incremental send can be performed.  

Once zrepi_ng has detected which mode to use (full or incremental) it performs the replication over SSH.

After the replication is complete zrepi_ng performs a cleanup of snaps older than the runtime specified max_snapage.  


## Assumptions
In no particular order:

1.  No attempt has been made to harden this zrepi_ng.  Since it is using subprocess.run, zrepi_ng has shell access as whatever user is running it.  USE AT YOUR OWN RISK!
2.  Some ZFS send/recv options are assumed.  Send: -p  Recv: -F -x sharenfs.
3.  The machine running zrepi_ng maintains the source data to replicate.
4.  Incremental sends are always used when possible.


## Tested on:
Freebsd, Fedora
